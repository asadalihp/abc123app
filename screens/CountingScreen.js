
import Carousel from 'react-native-snap-carousel';
import Entypo from 'react-native-vector-icons/Entypo';
import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, Dimensions,
    ImageBackground, StatusBar, TouchableOpacity, Button, FlatList
} from 'react-native';


const audioTests = [

    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/1.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/2.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/3.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/4.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/5.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/6.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/7.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/8.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/9.wav'),
    },
    {
        title: 'mp3 via require()',
        isRequire: true,
        url: require('../assets/sound/10.wav'),
    },

];

const images = [
    {
        pic1: require('../assets/images/01.png'),
        pic2: require('../assets/images/001.jpg'),
    },
    {
        pic1: require('../assets/images/02.png'),
        pic2: require('../assets/images/002.jpg'),
    },
    {
        pic1: require('../assets/images/03.png'),
        pic2: require('../assets/images/003.jpg'),
    }, {
        pic1: require('../assets/images/04.png'),
        pic2: require('../assets/images/004.jpg'),
    }, {
        pic1: require('../assets/images/05.png'),
        pic2: require('../assets/images/005.jpg'),
    }, {
        pic1: require('../assets/images/06.png'),
        pic2: require('../assets/images/006.jpg'),
    }, {
        pic1: require('../assets/images/07.png'),
        pic2: require('../assets/images/007.jpg'),
    }, {
        pic1: require('../assets/images/08.png'),
        pic2: require('../assets/images/008.jpg'),
    }, {
        pic1: require('../assets/images/09.png'),
        pic2: require('../assets/images/009.jpg'),
    }, {
        pic1: require('../assets/images/10.png'),
        pic2: require('../assets/images/010.jpg'),
    },
];






import Sound from 'react-native-sound';

var track = null;


type Props = {};
export default class App extends Component<Props> {
    // state hmesha constructor me inint hogi render method sy bahir, 

    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {


            steps: 1,


        };
    }





    playSound(testInfo) {
        const callback = (error, sound) => {
            if (error) {
                Alert.alert('error', error.message);
                return;
            }
            testInfo.onPrepared && testInfo.onPrepared(sound);
            sound.play(() => {
                sound.release();
            });
        };
        const sound = new Sound(testInfo.url, error => callback(error, sound));
    }






    render() {

        return (

            <View style={{ flex: 1 }}>

                <StatusBar hidden={true} />

                <ImageBackground source={require('../assets/images/4.jpg')}
                    style={{
                        flex: 1, backgroundColor: 'pink', alignItems: 'center',
                        justifyContent: 'space-between', flexDirection: 'row',
                    }} >


                    <View style={{ backgroundColor: 'white', borderRadius: 40, margin: 10 }}>

                        <TouchableOpacity
                            onPress={() => {
                                if (this._carousel.currentIndex > 0) {
                                    let info = audioTests[this._carousel.currentIndex - 1];
                                    this.playSound(info)
                                    this._carousel.snapToPrev()
                                }
                            }}>

                            <Entypo name="chevron-small-left" style={{ fontSize: 50, color: 'black', }} />

                        </TouchableOpacity>

                    </View>



                    <View style={{ flex: 1, }}>

                        <View style={{ flex: 1, }}>
                            <Carousel
                                // style={{flex:1, backgroundColor:'green'}}
                                ref={(c) => { this._carousel = c; }}
                                data={images}
                                renderItem={({ item, index }) => (
                                    <View style={{
                                        flex: 1,

                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                        marginBottom: -80,
                                        width: Dimensions.get("window").width - 140,
                                        alignItems: 'center'
                                    }}>
                                        <View style={{ margin: 2, flex: 1 }}>
                                            <Image source={item.pic1}
                                                style={{ width: '100%', height: '100%', borderRadius: 30 }}
                                                resizeMode={'center'} />
                                        </View>


                                        <View style={{ margin: 2, flex: 1 }}>
                                            <Image source={item.pic2}
                                                style={{ width: '100%', height: '100%', borderRadius: 30 }}
                                                resizeMode={'center'} />
                                        </View>
                                    </View>

                                )}
                                sliderWidth={Dimensions.get("window").width - 140}
                                itemWidth={Dimensions.get("window").width}
                            />
                        </View>

                        <View style={{ justifyContent: 'center', margin: 15, alignItems: 'center' }}>
                            <View style={{ backgroundColor: 'white', flexDirection: 'row', borderRadius: 70, }}>

                                <TouchableOpacity onPress={() => {
                                    let info = audioTests[this._carousel.currentIndex];
                                    this.playSound(info)
                                }}>

                                    <Entypo name="controller-play" style={{ fontSize: 50, color: '#525252', marginLeft: 7, marginTop: 2, marginBottom: 2 }} />
                                </TouchableOpacity>

                            </View>
                        </View>


                    </View>


                    <View style={{ backgroundColor: 'white', borderRadius: 40, margin: 10 }}>
                        <TouchableOpacity
                            onPress={() => {
                                if (this._carousel.currentIndex < audioTests.length - 1) {
                                    let info = audioTests[this._carousel.currentIndex + 1];
                                    this.playSound(info)
                                    this._carousel.snapToNext()
                                }
                            }}>

                            <Entypo name="chevron-small-right" style={{ fontSize: 50, color: 'black', }} />
                        </TouchableOpacity>
                    </View>

                </ImageBackground>


            </View>


        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    elevationLow: {
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 2,
            },
            android: {
                elevation: 5,
            },
        }),
    },
});



