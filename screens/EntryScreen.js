
import Carousel from 'react-native-snap-carousel';
import Entypo from 'react-native-vector-icons/Entypo';
import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Image, Dimensions,
    ImageBackground, StatusBar, TouchableOpacity, Button, FlatList
} from 'react-native';


import Sound from 'react-native-sound';

var track = null;


type Props = {};
export default class App extends Component<Props> {
    // state hmesha constructor me inint hogi render method sy bahir, 
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {


            steps: 1,


        };
    }





    playSound(testInfo) {
        const callback = (error, sound) => {
            if (error) {
                Alert.alert('error', error.message);
                return;
            }
            testInfo.onPrepared && testInfo.onPrepared(sound);
            sound.play(() => {
                sound.release();
            });
        };
        const sound = new Sound(testInfo.url, error => callback(error, sound));
    }






    render() {

        return (

            <View style={{ flex: 1 }}>
                <StatusBar hidden={true} />



                <ImageBackground source={require('../assets/images/4.jpg')}
                    style={{
                        flex: 1, alignItems: 'center',
                        justifyContent: 'space-between', flexDirection: 'row',
                    }} >
                    <View style={{ flexDirection: 'row', flex: 1 }}>


                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() => {
                                    this.props.navigation.navigate('CountingScreen')
                                }}>
                                <Image source={require('../assets/images/ac.jpg')}
                                    style={{ width: 400, height: 400, borderRadius: 30 }}
                                    resizeMode={'center'} />

                            </TouchableOpacity>
                        </View>


                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() => {
                                    this.props.navigation.navigate('AbcScreen');
                                }}>

                                <Image source={require('../assets/images/ac.jpg')}
                                    style={{ width: 400, height: 400, borderRadius: 30 }}
                                    resizeMode={'center'} />

                            </TouchableOpacity>
                        </View>



                        <View style={{ flex: 1 }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() => {
                                    let randomnumber = parseInt(Math.floor(Math.random() * 2) + 1);
                                    if (randomnumber === 1) {
                                        this.props.navigation.navigate('AbcScreen');
                                    } else {
                                        this.props.navigation.navigate('CountingScreen')
                                    }
                                }}>
                                <Image source={require('../assets/images/ac.jpg')}
                                    style={{ width: 400, height: 400, borderRadius: 30 }}
                                    resizeMode={'center'} />
                            </TouchableOpacity>
                        </View>



                    </View>

                </ImageBackground>
            </View>


        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    elevationLow: {
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 2,
            },
            android: {
                elevation: 5,
            },
        }),
    },
});



