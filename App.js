/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import {createStackNavigator, createAppContainer} from 'react-navigation';

import EntryScreen from './screens/EntryScreen';
import CountingScreen from './screens/CountingScreen';
import AbcScreen from './screens/AbcScreen';


 
const MainNavigator = createStackNavigator({
  EntryScreen: {screen: EntryScreen},
  CountingScreen: {screen: CountingScreen},
  AbcScreen: {screen: AbcScreen},
});

const App = createAppContainer(MainNavigator);

export default App;